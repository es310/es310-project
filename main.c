// ES310 Project: Temperature, Light and Humidity Sensors
// Chio Saephan, Jorge Cabrera, Scott Parmley
// Spring 2013

// CONFIGURATION CODE
#pragma config IESO=OFF, FCMEN=OFF, FOSC=INTIO7, PWRT=OFF, BOREN=OFF, WDTEN=OFF
#pragma config WDTPS=32768, MCLRE=ON, LPT1OSC=OFF, PBADEN=OFF, CCP2MX=PORTBE
#pragma config STVREN=OFF, LVP=OFF, XINST=OFF, DEBUG=OFF, CP0=OFF, CP1=OFF
#pragma config CP2=OFF, CP3=OFF, CPB=OFF, CPD=OFF, WRT0=OFF, WRT1=OFF
#pragma config WRT2=OFF, WRT3=OFF, WRTB=OFF, WRTC=OFF, WRTD=OFF, EBTR0=OFF
#pragma config EBTR1=OFF, EBTR2=OFF, EBTR3=OFF, EBTRB=OFF



/** D E C L A R A T I O N S **************************************************/

#define LED0 LATDbits.LATD0
#define LED1 LATDbits.LATD1
#define LED2 LATDbits.LATD2
#define LED3 LATDbits.LATD3
#define LED4 LATDbits.LATD4
#define LED5 LATDbits.LATD5
#define LED6 LATDbits.LATD6
#define LED7 LATDbits.LATD7

#define SW1	LATBbits.LATB0
#define	SW2 LATBbits.LATB1
#define	SW3 LATBbits.LATB2
#define	SW4 LATBbits.LATB3

/** I N C L U D E S **************************************************/
//include "LCD Display.c"
#include "p18f46k20.h"
#include <delays.h>
#include "oled.h"
#include "oled_interface.h"


/** V A R I A B L E S *************************************************/
#pragma udata   // declare statically allocated uinitialized variables
unsigned char highbyte;  // 8-bit variable
unsigned char lowbyte;  // 8-bit variable
unsigned char upperlowbyte;
unsigned char celcius;
unsigned char celciuslow;
unsigned char celciushigh;
unsigned char am2302resp;
unsigned char T_Byte1, T_Byte2, RH_Byte1, RH_Byte2,CheckSum;
unsigned short am2302humidity;
unsigned short am2302temperature;
unsigned char buffer[3];

/** D E C L A R A T I O N S *******************************************/
#pragma code    // declare executable instructions
void ADC_Convert(void);
void InitializeSystem(void);
void FindRA0(void);
void FindRA1(void);
void AM2302_Start(void);
unsigned char AM2302_GetResponse(void);
unsigned char getAM2302Byte(void);


void main(void)
{
	InitializeSystem();

	while(1){
		am2302resp = 0;			//  Reset AM2302 response Flag
		ADCON0=0b00000001;   	//  Reset AD Converter to Read TMP36GZ
		ADC_Convert();			//	Perform ADC Routing
		FindRA0();				// 	Perform Temperature Reading Handling Routine
		
		oled_init();			// 	Write Temperature Reading
		oled_putc_2x('T');
		oled_putc_2x('E');
		oled_putc_2x('M');
		oled_putc_2x('P');
		oled_putc_2x(' ');
		oled_putc_2x('=');
		oled_putc_2x(' ');
		oled_putc_2x(celciushigh+0x30);  //Have to add 0x30 to get the ASCII character
		oled_putc_2x(celciuslow+0x30);
		oled_putc_2x(' ');
		oled_putc_2x('o');
		oled_putc_2x('C');

		ADCON0=0b00000101;		// Reset ADC to Read from Input RA1
		ADC_Convert();			// Perform ADC Routine
		FindRA1();				// Perform Voltage Conversion
		oled_putc_2x('\n');		
		oled_putc_2x('L');
		oled_putc_2x('U');
		oled_putc_2x('M');
		oled_putc_2x(' ');
		oled_putc_2x(' ');
		oled_putc_2x('=');
		oled_putc_2x(' ');
		oled_putc_2x(highbyte+0x30);
		oled_putc_2x('.');
		oled_putc_2x(lowbyte+0x30);
		oled_putc_2x(' ');
		oled_putc_2x('V');		

		T_Byte1 = 0x00;					// Reset AM2302 Variables
		T_Byte2 = 0x00;
		RH_Byte1 = 0x00;
		RH_Byte2 = 0x00;
		CheckSum = 0x00;

		AM2302_Start();							// Perform data request
		am2302resp = AM2302_GetResponse();		// Validate acknowledge from Sensor

		if(am2302resp){							// Bit Banging Routine for each byte
			RH_Byte1 = getAM2302Byte();			
			RH_Byte2 = getAM2302Byte();			
			T_Byte1 = getAM2302Byte();
			T_Byte2 = getAM2302Byte();
			CheckSum = getAM2302Byte();
		}	
		
		if (CheckSum == (RH_Byte1 + RH_Byte2 + T_Byte1 + T_Byte2))  // Check Checksum
		{
			// convert measurements to decimal to display on oled
			am2302humidity = (0x0000+ RH_Byte1);					
			am2302humidity = (am2302humidity << 8) + RH_Byte2;
			am2302temperature = (0x000 + T_Byte1);
			am2302temperature = (am2302temperature << 8) + T_Byte2;

			oled_putc_2x('\n');		
			oled_putc_2x('T');
			oled_putc_2x('E');
			oled_putc_2x('M');
			oled_putc_2x('P');
			oled_putc_2x('2');
			oled_putc_2x('=');
			oled_putc_2x(' ');
			
			itoa(am2302temperature,buffer);  // integer to array
			oled_putc_2x(buffer[0]);
			oled_putc_2x(buffer[1]);
			oled_putc_2x('.');
			oled_putc_2x(buffer[2]);
			oled_putc_2x(' ');
			oled_putc_2x('o');
			oled_putc_2x('C');

			oled_putc_2x('\n');		
			oled_putc_2x('R');
			oled_putc_2x('H');
			oled_putc_2x('U');
			oled_putc_2x('M');
			oled_putc_2x('D');
			oled_putc_2x('=');
			oled_putc_2x(' ');
			
			itoa(am2302humidity,buffer);	//integer to array
			oled_putc_2x(buffer[0]);
			oled_putc_2x(buffer[1]);
			oled_putc_2x('.');
			oled_putc_2x(buffer[2]);
			oled_putc_2x(' ');
			oled_putc_2x('%');
		}
		oled_refresh();	

		Delay10KTCYx(256);				// Some delay before reading again
		Delay10KTCYx(256);
		Delay10KTCYx(256);
	}
} 											//end of main

void FindRA0(void)
{
	lowbyte = ADRESL;	//the lower byte of the adc converted voltage
	highbyte = ADRESH;	//the higher byte of the adc converted voltage
	celcius = 100*(ADRES*0.003222656)-50; // Arithmetic manipulation to get degree celcius
	

		celciushigh = celcius/10;		   	//Isolates the left digit
	
	if (celcius <10)					//Isolates the right digit. Range is 0-100 degree Celcius
		celciuslow = celcius - 0;
	else if (celcius <20)
		celciuslow = celcius - 10;
	else if (celcius <30)
		celciuslow = celcius - 20;
	else if (celcius <40)
		celciuslow = celcius - 30;
	else if (celcius <50)
		celciuslow = celcius - 40;
	else if (celcius <60)
		celciuslow = celcius - 50;
	else if (celcius <70)
		celciuslow = celcius - 60;
	else if (celcius <80)
		celciuslow = celcius - 70;
	else if (celcius <90)
		celciuslow = celcius - 80;
	else if (celcius <100)
		celciuslow = celcius - 90;
	
	if (celcius > 25)
		LATBbits.LATB2 = 1;
	else
		LATBbits.LATB2 = 0;
	
}


void FindRA1(void)
{
	lowbyte = ADRESL;	//the lower byte of the adc converted voltage
	highbyte = ADRESH;	//the higher byte of the adc converted voltage	

	// adjust the ADRES value so we can display the voltage on the oled
	if (ADRES < 0x136) //meaning less than 1 volt
	{ 
		highbyte = 0;	
		lowbyte = ADRES*0.0322; //adjust the decimal digit to a whole digit so we can display the decimal on the oled
	}
	if (ADRES >=0x136)
	{
		highbyte = 1;
		lowbyte = (ADRES-0x136)*0.0322;
	}	
}



void ADC_Convert(void)
{ // start an ADC conversion and return the 8 most-significant bits of the result
							// Use channel 0(AN0) to read the read the light sensor voltage and turn on the ADC
    ADCON0bits.GO_DONE = 1;             // start conversion
    while (ADCON0bits.GO_DONE == 1);    // wait for it to complete
}

void InitializeSystem(void)
{
	
	// Setup I/O Ports.
	TRISA = 0b00000011;
							
	TRISB = 0b11001111;
	PORTB = 0xFF;

	TRISC = 0b00000000;
	PORTC = 0;

	//LED Ooutput
	TRISD = 0x00;
	PORTD = 0;	
		
	TRISE = 0x02;
	PORTE = 0b00000010;

	// Setup oscillator Frequency	
	OSCCON = 0b01110100;

	//set as output to turn on motor
	TRISBbits.TRISB2 = 0;

	// Setup analog functionality	
	ANSEL = 0x00;							// Disabled all pins digital	
	ANSELbits.ANS0 = 1;						//RA0 will read temperature 
	ANSELbits.ANS1 = 1;						// RA1 will read light


	ADCON2=0b10101001;						// ADCON2 - bit7: 1 = right justfied, bits 5-1 acquisition time = 2 Tad, bits 2-0 ADC Clock Source F0SC /64
	ADCON1=0b00000000;						// Reference Vdd and Vss

    // Set up TIMER0 module, required to bit-bang AM2302
    INTCONbits.TMR0IF = 0;          // clear roll-over interrupt flag
    T0CON = 0b00001000;             // no prescale - increments every instruction clock.  PSA bit (bit 3) sets timer to not use prescaler.  Bit 6: use 8 bit counter.
    TMR0H= 0;                      // clear timer - always write upper byte first
    TMR0L = 0;

	//Configure MSSP for SPI master mode
	// Needed to drive oled

	SSPCON1 = 0b00110010;					// clock idle high, Clk /64
	SSPSTAT = 0b00000000;


} // end InitializeSystem

		
void AM2302_Start(void){
	TRISAbits.TRISA2 = 0;  		//Set Port RA2 as output
	PORTAbits.RA2 = 1;
	Delay1KTCYx(5);
	PORTAbits.RA2 = 0;			// Set output as low
	Delay1KTCYx(80);   			// 1KTCY = 250us, then delay 80000 cycles to get 20 milliseconds
	PORTAbits.RA2 = 1;			// Pull high ...	
	Delay10TCYx(8);				// For 20 microseconds
	TRISAbits.TRISA2 = 1;		// and change the PORTRA2 to input 
}



unsigned char AM2302_GetResponse(void){
	
	T0CONbits.TMR0ON = 0;
    TMR0H = 0;									// clear timer - always write upper byte first
    TMR0L = 0;
	INTCONbits.TMR0IF = 0;
    T0CONbits.TMR0ON = 1;							// start timer

	// get value on port RA2
 	while(!PORTAbits.RA2 && !INTCONbits.TMR0IF);	// If there's no response within 256us, the Timer2 overflows
  
	if (INTCONbits.TMR0IF){ 
		PORTD = 2;
		return 0;    			// There was a timer overflow, that means we did not get a response
	}
  	else {											// So sensor pulled signal high, now let's check for signal low
		T0CONbits.TMR0ON = 0;
		INTCONbits.TMR0IF = 0;						// clear roll-over interrupt flag
    	TMR0H = 0;									// clear timer - always write upper byte first
    	TMR0L = 0;
		T0CONbits.TMR0ON = 1;
  		while(PORTAbits.RA2 && !INTCONbits.TMR0IF);
   		if (INTCONbits.TMR0IF){
			PORTD = 4;	
			return 0;			// signal never changed to high, damn you sensor!
   		}
		else {
			PORTD = 1;
    		T0CONbits.TMR0ON = 0;					// we got a response! turn the timer off.
    		return 1;								// return success
   		}
  	}
}


unsigned char getAM2302Byte(void){
	unsigned char num = 0,i;
	T0CONbits.TMR0ON = 0;	
	TMR0H = 0;
	TMR0L = 0;
	INTCONbits.TMR0IF = 0;
	T0CONbits.TMR0ON = 1;							// start timer

	for (i=0; i<8; i++){
		while(!PORTAbits.RA2 && !INTCONbits.TMR0IF);  //adding TMR0IF as a safe 
		if (!INTCONbits.TMR0IF) // we should be now on a high, then delay 40 microseconds, if still high, make it a one, if not make it a zero
		Delay10TCYx(16);				// Delay 40 microseconds
		if(PORTAbits.RA2){
			num |= 1<<(7-i);
		}
   		while(PORTAbits.RA2);  // wait until this bit finishes.
   	}
  	return num;
}